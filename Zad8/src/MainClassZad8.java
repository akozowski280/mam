
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import org.pcj.NodesDescription;
import org.pcj.PCJ;
import org.pcj.PcjFuture;
import org.pcj.RegisterStorage;
import org.pcj.StartPoint;
import org.pcj.Storage;
import java.lang.Math;

@RegisterStorage(MainClassZad8.Shared.class)
public class MainClassZad8 implements StartPoint {

    @Storage(MainClassZad8.class)
    enum Shared {
        u, uf       // u i uf to wartosci calki odpowiednio typu double i float                
    }
    //zadeklarowanie tablic przechowujaych wartosci calek 
    double u[];
    float uf[];
    
    //Inicjalizacja obliczen rownoleglych
    public static void main(String[] args) throws IOException {
      String nodesFile = "nodes.txt";
       PCJ.executionBuilder(MainClassZad8.class)
                             .addNodes(new File("nodes.txt"))
                             .start();
       // plik "nodes.txt" zawiera informacj� na temat liczby i nazwy rdzeni wykorzystanych 
       //podczas obliczen
    }
    //Rozpoczecie obliczen
    @Override
    public void main() throws Throwable {

    	// Program liczy wartosc calki dla 30 roznych krokow calkownania
    	//Po wyznaczeniu calki liczba krokow jest podwajana po kazdej iteracji
        int N = 30, index = 0;
        double ns = 1,x,c;
        double d = 1;
        
        float df = 1;
        float nf,xf,cf;
  
        u = new double[N+1];
        uf = new float[N+1];
        double[] diff = new double[N+1];
        
        //ze wzgledu na stosunkowo krotki czas wykonania (oraz unikniecia jego wydluzenia przy tak 
        //malej liczbie punktow) pierwsze 10 iteracji zostanie policzona tylko na jednym watku
        if (PCJ.myId() == 0) {
        	while(index<=10)
        	{
        		//wartosc calki w danym kroku czasowym
        		c = 0;
        		cf = 0;
        		for(int i = 0 ; i<ns; i++)
        		{
        			//calka double
        			x = (i+0.5)*d;
        			c += Math.sin(x) * d;
        			//calka float
        			xf = (float) ((i+0.5)*df);
        			cf += Math.sin(x) * df;
        		}
        		//Zapisanie wartosci calki w tablicy
        		u[index] = c;
        		uf[index] = cf;
        		//aktualizacja krokow calkowania
        		ns *= 2;
        		nf = (float) ns;
        		df = 1 / nf;
        		d = 1 / ns;
        		//Przejscie do kolejnej iteracji
        		index++;
        	}
        	
        }
        else // Wyzerowanie wartosci calki na pozostalych watkach
        	while(index<=10)
        	{
        		u[index] = 0;
        		uf[index] = 0;
        		index++;
        	}
        //Synchroniazcja watkow
        PCJ.barrier();
        
        //podzial obliczen na watki
        int n = (int) Math.pow(2, index);
        int np = (int) (n / PCJ.threadCount());        	// ilosc elementow calkowanych przez watek
        int ip = PCJ.myId() * np;                   	// poczatek zakresu calkowania
        int ik = (PCJ.myId() + 1) * np;					// konbiec zakresu calkowania 
        double dp = 1.0/n;								// krok calkowania
        df = (float) (1.0/n);

        long czas = System.nanoTime();					// pomiar czasu obliczen
        
        //Kazdy z watkow otrzymal zakres o tej samej liczbie krokow calkowania
        while(index<=N)
        {
        	c = 0;
        	cf = 0;
    		for(int i = ip ; i<ik; i++)
    		{
    			x = (i+0.5)*dp;
    			c += Math.sin(x) * dp;
    			
    			xf = (float) ((i+0.5)*df);
    			cf += Math.sin(xf) * df;
    		}
    		u[index] = c;
    		uf[index] = cf;
    		n =n*2;
    		np = n / PCJ.threadCount();
    		ip = PCJ.myId() * np; 
    		ik = (PCJ.myId() + 1) * np;
    		dp = 1.0/n;
    		df = (float) (1.0/n);
    		index++; 	
        }

        czas = System.nanoTime() - czas;                      // koniec pomiaru czasu wykonania
        
        // Po wykonaniu obliczen nale�y zebrac wyniki do watku 0, zsumowac i na tej podstawie 
        // zap[isac wartosci calek dla poszczegolnych krokow do pliku "wyniki.txt"
        if(PCJ.myId()==0)
        {
        		String s1,s2,diff_s;
        	    String fileName = "wyniki.txt"; // utworzenie pliku
				FileWriter fileWriter = new FileWriter(fileName);
        	    PrintWriter printWriter = new PrintWriter(fileWriter);
        	    
	        	for(int i = 0; i<=N;i++)
	        	{
	        		//Zebranie oraz zsumowanie wartosci calki policzonej na kazdym watku dla 
	        		//danego kroku czasowego
	        		 for (int p = 1; p < PCJ.threadCount(); p++) {
	                     u[i] = u[i] + (double) PCJ.get(p, Shared.u, i);
	                     uf[i] = uf[i] + (float) PCJ.get(p, Shared.uf, i);
	                     diff[i] = Math.abs(u[i] - uf[i]);
	        		 }
	        		 //koonwersja danych to typu String
	        		  s1 = Double.toString(u[i]);
	        		  s2 = Float.toString(uf[i]);
	        		  diff_s = Double.toString(diff[i]);
	        		  //zapisanie do pliku
	        		 printWriter.printf("%d, %s, %s, %s\n",i,s1,s2,diff_s);
	        		 System.out.println("Solution for " + i + " : " +u[i] + " " + uf[i] + " " + diff[i]);
	        	}
        	printWriter.close();
        }
        PCJ.barrier(); 
        System.out.println(PCJ.myId() + " czas "+ czas/1000_000);
        
    }
}

 